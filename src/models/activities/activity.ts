import { Group } from "../program/group";
import { Time } from "@angular/common/src/i18n/locale_data_api";

export interface Activity {
    title: string;
    author?: string;
    //todo group is an Object itself
    groups: Group[];
    timeStart: Time;
    timeEnd: Time;
    description?: string;
    photos?: any[];
    attachments?: any[];
    links?: string[];
    mandatory?: boolean;
    //todo change Activity to an Interface, define all classes from the types
    type: ActivityType
}

export enum ActivityType {
    LESSON = 1,
    EXCURSION,
    MEETING,
    PERFORMANCE,
    HOLIDAY,
    CELEBRATION,
    PARTY,
}