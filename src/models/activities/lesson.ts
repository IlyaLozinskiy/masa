import { Activity, ActivityType } from './activity';
import { Group } from '../program/group';
import { Time } from '@angular/common';

export class Lesson implements Activity {
    title: string;
    author?: string;
    groups: Group[];
    timeStart: Time;
    timeEnd: Time;
    description?: string;
    photos?: any[];
    attachments?: any[];
    links?: string[];
    mandatory?: boolean;
    type: ActivityType;
    number: number;
    teacher: string;
    topic: string;
    homeTask: string;
}