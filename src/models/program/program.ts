import { Group } from "./group";

export class Programm {
    name: string;
    dateStart: string;
    dateEnd: string;
    location: string;
    companyName?: string;
    groups: Group[];
}