import { Group } from "./group";

export class Madrih{
    id: string;
    name: string;
    lastName: string;
    phone: string;
    email: string;
    group: Group;
}