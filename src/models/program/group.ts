import { Pupil } from "./pupil";
import { Madrih } from "./madrih";

export class Group {
    id: number;
    name: string;
    madrih: Madrih;
    pupils: Pupil[];
    constructor(_id?: number, _name?: string, _madrih?: Madrih, _pupils?: Pupil[]) {
        if (_id) this.id = _id;
        if (_name) this.name = _name;
        if (_madrih) this.madrih = _madrih;
        if (_pupils) this.pupils = _pupils;
        else this.pupils = new Array<Pupil>();
    }
}