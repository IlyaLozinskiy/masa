import { Group } from "./group";
import { User } from "../user/user";

export class Pupil extends User {
    groupId: number;
    constructor(id?, name?, lastName?, email?, phone?, _groupId?) {
        super(id, name, lastName, email, phone);
        if (_groupId) this.groupId = _groupId;
    }

}