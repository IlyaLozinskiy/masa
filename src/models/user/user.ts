export class User {
    id: string;
    name: string;
    lastName: string;
    email: string;
    phone: string;

    constructor(_id?, _name?, _lastName?, _email?, _phone?) {
        if (_id !== undefined) {
            this.id = _id;
            this.email = _email;
            this.name = _name;
            this.lastName = _lastName;
            this.phone = _phone;
        }
    }
}