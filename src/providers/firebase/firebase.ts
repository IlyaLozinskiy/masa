import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase';



@Injectable()
export class FirebaseProvider {

  constructor(private firebase: Firebase) {
    console.log('Hello FirebaseProvider Provider');
  }

  getToken() {
    return this.firebase.getToken()
  }

  onTokenRefresh() {
    return this.firebase.onTokenRefresh()
  }

  grantPerm() {
    return this.firebase.grantPermission();
  }

  onNotificationArrived() {
    return this.firebase.onNotificationOpen();
  }

  subscribe(topic: string) {
    return this.firebase.subscribe(topic);
  }

  unsubscribe(topic: string) {
    return this.firebase.unsubscribe(topic);
  }

  unregister() {
    return this.firebase.unregister();
  }
}
