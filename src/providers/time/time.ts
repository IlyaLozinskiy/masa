import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Activity } from '../../models/activities/activity';
import { HttpClient } from '@angular/common/http';
declare var require: any


@Injectable()
export class TimeProvider {

  private fs = require('fs');

  constructor(private http: HttpClient) {
    console.log('Hello TimeProvider Provider');
  }

  getTimetable(){
    return this.http.get('assets/restApi.json').map(res => (<any>res).timetable)
  }

  getActivityFiles(): Observable<any> {
    var response = {
      files: [
        {
          title: "example0.png",
          url: "http://wallfuse.com/web/wallpapers/302/3750x1200.jpg",
          type: "image",
          size: 1024
        },
        {
          title: "example1.png",
          url: "./assets/imgs/pl.png",
          type: "image",
          size: 1024
        },
        {
          title: "example2.png",
          url: "http://wallfuse.com/web/wallpapers/302/3750x1200.jpg",
          type: "image",
          size: 1024
        },
        {
          title: "example3.png",
          url: "./assets/imgs/pl.png",
          type: "image",
          size: 1024
        },
        {
          title: "example4.png",
          url: "http://wallfuse.com/web/wallpapers/302/3750x1200.jpg",
          type: "image",
          size: 1024
        }
      ]
    }
    let obs = new Observable(observer => {
      setTimeout(() => {
        observer.next(response);
      }, 1500);
      setTimeout(() => {
        observer.complete();
      }, 2500);
    });
    return obs;
  }

  getTodayActivities(): Observable<any> {
    var response = {
      todayActivities: [
        {
          title: 'Hebrew',
          groups: ['Java'],
          description: "Past Tense",          
          timeStart: {
            hours: 9,
            minutes: 0
          },
          timeEnd: {
            hours: 13,
            minutes: 0
          },
          type: 1,
          homeTask: 'Pages 128-140, Tasks 1-7, Text 1 (read & translate)',
          teacher: 'Vladimir Shapiro',
          topic: 'Past Tense. Nouns, Pronouns, Adjectives, Verbs',
          number: 12,
          mandatory: true,
          links: ['https://drive.google.com/1231wdqsd12fsd', 'https://google.com', 'https://yandex.ru'],
          attachments: new Array<any>()
        },
        {
          title: 'Java',
          groups: ['Java'],
          description: "Intoduction to Java Collections",
          timeStart: {
            hours: 14,
            minutes: 0
          },
          timeEnd: {
            hours: 18,
            minutes: 0
          },
          type: 1,
          attachments: new Array<any>()
        },
        {
          title: 'Meeting',
          groups: ['Java'],
          description: "Meeting, that will show your future",
          timeStart: {
            hours: 19,
            minutes: 0
          },
          timeEnd: {
            hours: 20,
            minutes: 0
          },
          type: 3,
          attachments: new Array<any>()
        }
      ]
    }
    let obs = new Observable(observer => {
      this.getFiles('./assets/test.txt').then((file) => {
        response.todayActivities[0].attachments[0] = file;

        this.getFiles('./assets/imgs/pl.png').then((file1) => {
          response.todayActivities[0].attachments[1] = file1;

          this.getFiles('./assets/audio.m4a').then((file2) => {
            response.todayActivities[0].attachments[2] = file2;
            observer.next(response);
          }).catch((error) => {
            console.error(error);
          });

        }).catch((error) => {
          console.error(error);
        });

      }).catch((error) => {
        console.error(error);
        observer.error(error);
      });
      setTimeout(() => {
        observer.complete();
      }, 100);
    });
    return obs;
  }


  //todo: load file only when user opens it
  private getFiles(path: string) {
    var prom = new Promise((resolve, reject) => {
      var rawFile = new XMLHttpRequest();
      rawFile.open("GET", path, false);
      rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4) {
          if (rawFile.status === 200 || rawFile.status == 0) {
            var path = rawFile.responseURL;
            var name = path.substring(path.lastIndexOf('/') + 1, path.length);
            var type = name.substring(name.lastIndexOf('.') + 1, name.length);
            var file = {
              content: rawFile.responseText,
              name: name,
              type: type
            }
            resolve(file);
          } else {
            reject();
          }
        }
      }
      rawFile.send(null);
    })
    return prom;
  }
}
