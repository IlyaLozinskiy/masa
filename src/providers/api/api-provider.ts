import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "../../models/user/user";
import { Observable } from "rxjs";
import { Group } from "../../models/program/group";
import { Pupil } from "../../models/program/pupil";

@Injectable()
export class ApiProvider {
    constructor(private http: HttpClient) {

    }

    public getGroup(): Observable<Group> {
        let obs = new Observable<Group>(observer => {
            let group = new Group(0,"Java");
            for (let i = 0; i < 10; i++) {
                group.pupils.push(new Pupil(
                    i,
                    "Name-" + i,
                    "Lastname-" + i,
                    "example-" + i + "@gmail.com",
                    "0587135252",
                    group.id
                ));
            }
            observer.next(group);
            observer.complete();
        });
        return obs
    }
}