import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

const DARKSKY_KEY: String = "6ddf7a532d3645fbb1a2b72311d00eca";
const GOOGLE_KEY: String = 'AIzaSyB0kuSd4mhMPOsKJuO7Amff5AjIYtln4yI';
@Injectable()
export class GeocodeService {

    constructor(public http: Http) { }

    locate(address) {
        return new Promise((resolve, reject) => {
            this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + encodeURIComponent(address) + '&key=' + GOOGLE_KEY)
                .map(res => res.json())
                .subscribe(data => {
                    console.log("weather arrived: ", data);
                    if (data.status === "OK") {
                        resolve({
                            name: data.results[0].formatted_address, location: {
                                latitude: data.results[0].geometry.location.lat,
                                longitude: data.results[0].geometry.location.lng
                            }
                        });
                    } else {
                        reject(data);
                    }
                }, error => {
                    reject(error);
                });
        });
    }

    getWeather(latitude: String, longitude: String) {
        return this.http.get('https://api.forecast.io/forecast/' + DARKSKY_KEY + '/' + latitude + ',' + longitude + '?exclude=alerts,minutely,hourly')
            .map(res => res.json());

    }
}