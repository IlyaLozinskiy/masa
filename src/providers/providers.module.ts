import { NgModule } from '@angular/core';
import { TimeProvider } from '../providers/time/time';
import { Observer } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { GeocodeService } from './weather/weather';
import { HttpModule } from '@angular/http';
import { FirebaseProvider } from './firebase/firebase';
import { Firebase } from '@ionic-native/firebase';
import { HttpClientModule } from '@angular/common/http';
import { ApiProvider } from './api/api-provider';

@NgModule({
    declarations: [],
    imports: [
        HttpModule,
        HttpClientModule,
    ],
    bootstrap: [],
    entryComponents: [],
    providers: [
        TimeProvider,
        AndroidFullScreen,
        StatusBar,
        InAppBrowser,
        GeocodeService,
        FirebaseProvider,
        Firebase,
        ApiProvider
    ]
})
export class ProvidersModule {
}