import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the MinutesPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'minutes',
})
export class MinutesPipe implements PipeTransform {

  transform(value: string) {
    if (value === '0') {
      return '00';
    } else if (Number.parseInt(value) < 10) {
      return '0' + value;
    } else {
      return value;
    }
  }
}
