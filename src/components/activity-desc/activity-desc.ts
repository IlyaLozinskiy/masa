import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Activity } from '../../models/activities/activity';
import { FileViewerComponent } from '../fileviewer/fileviewer';
import { TimeProvider } from '../../providers/time/time';
import { BasePage } from '../../pages/basepage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ColorProvider } from '../../services/color';

/**
 * Generated class for the ActivityDescComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'activity-desc',
  templateUrl: 'activity-desc.html'
})
export class ActivityDescComponent extends BasePage {

  files: any[];

  filesLoading: boolean;

  loadCount: number = 0;

  activity: Activity;

  numbers = [0, 1, 2, 3, 4];

  constructor(
    private colors: ColorProvider,
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private timeProvider: TimeProvider,
    private iab: InAppBrowser) {
    super();
    this.activity = this.navParams.data.activity;
    this.files = new Array<any>();
    this.filesLoading = true;
  }

  ngOnInit() {
    this.getFiles()
    console.log(this.activity);
    
  }

  ionViewDidLoad(){
    var header = <HTMLElement>document.getElementsByTagName('activity-desc')[0].children[0].children[0].children[0];
    header.style.background = this.colors.getColor(this.activity.type);
  }

  private getFiles() {
    let sub = this.timeProvider.getActivityFiles().subscribe(response => {
      console.log(response);
      this.files = response.files;
    }, error => {
      console.error(error);
    });
    this.subscribtions.push(sub);
  }

  private openLink(link) {
    console.log("opening " + link + " ...");
    this.iab.create(link, "_system", "");
  }

  private dismiss() {
    this.viewCtrl.dismiss();
  }

  

}
