import { NgModule } from '@angular/core';
import { ActivityDescComponent } from './activity-desc/activity-desc';
import { NavParams, IonicApp, IonicModule } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { DateViewComponent } from './dateview/dateview';
import { SpinnerComponent } from './spinner/spinner';
import { FileViewerComponent } from './fileviewer/fileviewer';
import { TimeLayoutComponent } from './time-layout/time-layout';
import { PipesModule } from '../pipes/pipes.module';
import { AttachmentsComponent } from './attachments/attachments';
import { HomeworkComponent } from './homework/homework';
import { DescriptionComponent } from './description/description';
import { HeaderComponent } from './header/header';
import { ProfileComponent } from './profile/profile';
@NgModule({
	declarations: [
		ActivityDescComponent,
		DateViewComponent,
		SpinnerComponent,
		FileViewerComponent,
		TimeLayoutComponent,
		AttachmentsComponent,
		HomeworkComponent,
		DescriptionComponent,
    HeaderComponent,
    ProfileComponent],

	imports: [PipesModule, IonicModule],

	entryComponents: [
		ActivityDescComponent,
		DateViewComponent,
		FileViewerComponent,
		TimeLayoutComponent,
		AttachmentsComponent,
		HomeworkComponent,
		DescriptionComponent,
		ProfileComponent
	],

	exports: [
		ActivityDescComponent,
		DateViewComponent,
		SpinnerComponent,
		FileViewerComponent,
		TimeLayoutComponent,
		AttachmentsComponent,
		HomeworkComponent,
		DescriptionComponent,
    HeaderComponent,
    ProfileComponent]
})
export class ComponentsModule {
}
