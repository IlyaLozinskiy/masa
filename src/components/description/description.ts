import { Component, Input } from '@angular/core';

/**
 * Generated class for the DescriptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'description',
  templateUrl: 'description.html'
})
export class DescriptionComponent {

  @Input('description')  description: string;

  constructor() {
  }

  ngOnInit(){
    console.log("desc: ",this.description);
    
  }
}
