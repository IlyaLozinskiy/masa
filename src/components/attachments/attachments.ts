import { Component, Input } from '@angular/core';
import { FileViewerComponent } from '../fileviewer/fileviewer';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

/**
 * Generated class for the AttachmentsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'attachments',
  templateUrl: 'attachments.html'
})
export class AttachmentsComponent {

  @Input('filesLoading') filesLoading: boolean;
  @Input('files') files: any[];
  @Input('numbers') numbers: any[];

  loadCount: number = 0;

  constructor(private modalCtrl: ModalController) {
  }

  ngOnInit() {
  }

  private load(e) {
    this.loadCount++;
    if (this.loadCount == this.files.length) {
      this.filesLoading = false;
    }
  }

  private openFile(number) {
    if(!this.filesLoading) this.modalCtrl.create(FileViewerComponent, { number: number, files: this.files }).present();
  }

}
