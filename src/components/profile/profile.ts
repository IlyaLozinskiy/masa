import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular/navigation/view-controller';

/**
 * Generated class for the ProfileComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'profile',
  templateUrl: 'profile.html'
})
export class ProfileComponent {

  changePass: boolean;

  constructor(private viewCtrl: ViewController) {
  }

  /*******Profile*******/
  private save() {

  }

  private cancel() {
    this.changePass = false;
  }

  private fileUpload(event) {
    console.log(event);
  }

  private showPass() {
    this.changePass = true;
  }

  private dismiss() {
    this.viewCtrl.dismiss();
  }
}
