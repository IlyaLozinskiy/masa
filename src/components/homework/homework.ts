import { Component, Input } from '@angular/core';

/**
 * Generated class for the HomeworkComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'homework',
  templateUrl: 'homework.html'
})
export class HomeworkComponent {

  @Input('homeTask') homeTask: string;

  constructor() {
  }

}
