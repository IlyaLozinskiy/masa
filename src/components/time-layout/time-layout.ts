import { Component, Input } from '@angular/core';
import { Activity } from '../../models/activities/activity';

/**
 * Generated class for the TimeLayoutComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'time-layout',
  templateUrl: 'time-layout.html'
})
export class TimeLayoutComponent {

  @Input('activity') activity: Activity;

  constructor() {
  }

  ngOnInit() {
  }
}
