import { Component, Input } from '@angular/core';
import { Time } from '@angular/common';

/**
 * Generated class for the DateviewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'dateview',
  templateUrl: 'dateview.html'
})
export class DateViewComponent {

  @Input('time') time: Time;
  hours: number;
  minutes: number;

  constructor() {
  }

  ngOnInit() {
    this.hours = this.time.hours;
    this.minutes = this.time.minutes;
  }
}
