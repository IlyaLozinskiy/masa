import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular/util/events';


@Component({
  selector: 'header',
  templateUrl: 'header.html'
})
export class HeaderComponent {

  @Input('name') name;
  @Input('lastname') lastName;


  constructor(private events: Events) {
  }

  ngOnInit() {
    console.log('Hello HeaderComponent Component', this.name, this.lastName);
  }

  private openProfile() {
    console.log("OPEN")
    this.events.publish("open:profile");
  }
}
