import { Component, ViewChild } from '@angular/core';
import { NavParams, ViewController, Slides } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the FileviewerComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'fileviewer',
  templateUrl: 'fileviewer.html'
})
export class FileViewerComponent {

  @ViewChild(Slides) slides: Slides;

  files: any[];

  currentFile: number;

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private statusBar: StatusBar) {
    this.currentFile = this.navParams.data.number;
    this.files = this.navParams.data.files;
  }

  ngOnInit() {
    this.statusBar.backgroundColorByHexString("#000000");
  }
  
  ionViewWillEnter() {
    this.slides.slideTo(this.currentFile,1);
  }

  ionViewWillLeave(){
    this.statusBar.backgroundColorByHexString("#ffffff");
  }

  private hideDesc(){
    var disappearingEl = document.querySelectorAll('.disap');
    disappearingEl[0].classList.toggle("closed");
    disappearingEl[1].classList.toggle("closed");
  }

  private slideChanged() {
    var index = this.slides.getActiveIndex();
    if(index == this.files.length) return;
    this.currentFile = index;
  }

  private dismiss() {
    this.viewCtrl.dismiss();
  }

}
