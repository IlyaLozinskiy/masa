import { CallNumber } from "@ionic-native/call-number";
import { Injectable } from "@angular/core";


@Injectable()
export class CallService {
    constructor(private callNumber: CallNumber) {

    }

    public doCall(number: string) {
        this.callNumber.callNumber(number, true).then((response) => {
            console.log("calling number " + number +  ": ",response);
        }).catch((error) => {
            console.error("error while calling: ",error);
        });
    }
}