import { Injectable } from "@angular/core";
import { Platform } from "ionic-angular/platform/platform";
import { LaunchNavigator, LaunchNavigatorOptions } from "@ionic-native/launch-navigator";


@Injectable()
export class OpenMaps {
    constructor(
        private launchNavigator: LaunchNavigator,
        private platform: Platform
    ) { }

    // a { string } containing the address.e.g."Buckingham Palace, London"
    // a { string } containing a latitude/longitude coordinate. e.g. "50.1. -4.0"
    // an { array }, where the first element is the latitude and the second element is a longitude,
    // as decimal numbers.e.g. [50.1, -4.0]

    public openApp(dest: string) {
        if (!this.platform.is("cordova")) {
            console.warn('not cordova');
            return;
        }
        let options: LaunchNavigatorOptions = {
            successCallback: (e) => {
                console.log("succes: ", e)
            },
            errorCallback: (e) => {
                console.log("error: ", e)
            }
        };

        this.launchNavigator.navigate(dest, options)
            .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
            );
    }
}