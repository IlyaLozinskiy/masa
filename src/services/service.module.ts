import { NgModule } from '@angular/core';
import { CallNumber } from "@ionic-native/call-number";
import { LaunchNavigator } from "@ionic-native/launch-navigator";
import { CallService } from './call';
import { OpenMaps } from './maps';
import { ColorProvider } from './color';

@NgModule({
    declarations: [],
    imports: [],
    bootstrap: [],
    entryComponents: [],
    providers: [
        CallNumber,
        CallService,
        LaunchNavigator,
        OpenMaps,
        ColorProvider
        
    ]
})
export class ServicesModule {
}