import { Injectable } from '@angular/core';
import { ActivityType } from '../models/activities/activity';

/*
  Generated class for the ColorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ColorProvider {

  colors = ['#87ceeb', '#23da59', '#f9f94f', '#9370db', '#888282', '#f39d00', '#f39d00'];


  constructor() {
    console.log('Hello ColorProvider Provider');
  }

  getColor(type: ActivityType) {
    console.debug("1");
    return `linear-gradient( to bottom right,${this.colors[type - 1]} 20%, white 90%)`;
  }

}
