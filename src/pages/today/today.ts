import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ModalOptions } from 'ionic-angular';
import { TimeProvider } from '../../providers/time/time';
import { BasePage } from '../basepage';
import { Activity } from '../../models/activities/activity';
import { ActivityDescComponent } from '../../components/activity-desc/activity-desc';
import { GeocodeService } from '../../providers/weather/weather';
import { ColorProvider } from '../../services/color';

/**
 * Generated class for the TodayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-today',
  templateUrl: 'today.html',
})
export class TodayPage extends BasePage {

  dataLoading: boolean;

  activities: Activity[];

  currentDate: Date;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private timeService: TimeProvider,
    private modalCtrl: ModalController,
    private colors: ColorProvider,
    private weather: GeocodeService) {
    super();
    this.activities = new Array<Activity>();
    this.currentDate = this.navParams.data.date;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodayPage with params: ', this.navParams.data);
    this.getTodayActivities();
    this.locate();
  }

  private getTodayActivities(refresher?) {
    this.dataLoading = true;
    let sub = this.timeService.getTodayActivities().subscribe(response => {
      this.activities = response.todayActivities;
    }, error => {
      console.error(error);
    }, () => {
      this.dataLoading = false;
      if (refresher) refresher.complete();
    });
    this.subscribtions.push(sub);
  }

  private locate() {
    this.weather.locate('Ayanot').then((response) => {
      console.log(response);
      this.loadWeather(response);
    }).catch((error) => {
      console.error(error);
    });
  }

  private loadWeather(location) {
    this.weather.getWeather(location.location.latitude, location.location.longitude).subscribe(response => {
      console.log(response);

    }, error => {
      console.error(error);
      
    }, () => {

    });
  }

  private doRefresh(refresher) {
    this.activities = new Array<Activity>();
    this.currentDate = new Date();
    this.getTodayActivities(refresher);
  }

  private openDesc(act: Activity) {
    let opt: ModalOptions = {
      showBackdrop: false,

    }
    this.modalCtrl.create(ActivityDescComponent, { activity: act }, opt).present();
  }

  private getIconName(type) {
    switch (type) {
      case 1:
        return 'school';
      case 2:
        return 'people';
      default:
        break;
    }
  }

}
