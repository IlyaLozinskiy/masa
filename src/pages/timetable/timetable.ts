import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BasePage } from '../basepage';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { TimeProvider } from '../../providers/time/time';
import { Activity } from '../../models/activities/activity';
import { Element } from '@angular/compiler';


@Component({
  selector: 'page-timetable',
  templateUrl: 'timetable.html',
})
export class TimetablePage extends BasePage {

  activitiesLoadingComplete: boolean;

  eventSource = [];

  viewTitle: string;

  selectedDay = new Date();

  calendar = {
    mode: 'month',
    currentDate: new Date()
  };

  colors = ['#87ceeb', '#23da59', '#f9f94f', '#9370db', '#888282', '#f39d00', '#f39d00']

  activitiesToShow: Activity[];
  DatesActivities: Map<string, Activity[]>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private timeService: TimeProvider) {
    super();
    console.clear();
    this.DatesActivities = new Map<string, Activity[]>();
    this.activitiesToShow = new Array<Activity>();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TimetablePage');
  }

  ionViewDidEnter() {
  }

  private addEventFromServer(activity: Activity) {

    let eventData = {
      startTime: new Date(2018, 6, Math.random() + Math.random() + 1),
      endTime: new Date()
    };

    let events = this.eventSource;
    events.push(eventData);
    this.eventSource = [];
    setTimeout(() => {
      this.eventSource = events;
    });
  }


  private addEvent() {
    let modal = this.modalCtrl.create('EventModalPage', { selectedDay: this.selectedDay });
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        let eventData = data;

        eventData.startTime = new Date(data.startTime);
        eventData.endTime = new Date(data.endTime);

        let events = this.eventSource;
        events.push(eventData);
        this.eventSource = [];
        setTimeout(() => {
          this.eventSource = events;
        });
      }
    });
  }

  private onViewTitleChanged(title) {
    this.viewTitle = title;
    let month = title.substring(0, title.indexOf(' '));
    let year = title.substring(title.indexOf(' ') + 1, title.length);
    // todo send month&year to the server to fetch data for the month that is currently shown a the user;
    this.getTimetable();
  }

  private onEventSelected(event) {
    let start = moment(event.startTime).format('LLLL');
    let end = moment(event.endTime).format('LLLL');

    let alert = this.alertCtrl.create({
      title: '' + event.title,
      subTitle: 'From: ' + start + '<br>To: ' + end,
      buttons: ['OK']
    })
    alert.present();
  }

  private onTimeSelected(ev) {
    this.selectedDay = ev.selectedTime;
    if (this.activitiesLoadingComplete) {
      this.activitiesToShow = this.DatesActivities.get(this.selectedDay.toLocaleDateString())
    }
  }

  private getTimetable() {
    //todo 
    // 1) check local storage for the flag showing that the user has already fetched timetable dara from the server
    // 2) check login response for the flag that show if there were any changes
    if (true == true) {
      this.activitiesLoadingComplete = false;
      this.timeService.getTimetable().subscribe(response => {
        //  console.log("timetable: ", response);
        this.fillCalendar(response);
      }, error => {
        console.error(error);

      }, () => {

      });
    }
  }

  private async fillCalendar(response) {
    let cells = document.getElementsByTagName('td');
    response.forEach(oneDay => {
      this.checkTableCells(cells, oneDay);
      if (response.indexOf(oneDay) == response.length - 1) {
        this.activitiesLoadingComplete = true;
      }
    });
  }

  private checkTableCells(cells: NodeListOf<HTMLTableDataCellElement>, oneDayFromServer: any) {
    var day = this.getDayValue(oneDayFromServer.date);
    for (var i = 0; i < cells.length; i++) {
      if (this.isVisible(cells[i]) && cells[i].innerText == day) {
        this.setCellColor(cells[i], oneDayFromServer.activities, day);
      }
    }
  }

  private getDayValue(date: string) {
    let day: string = date.substring(0, date.indexOf('.'));
    day = day.startsWith('0') ? day.substring(1) : day;
    return day;
  }

  private async setCellColor(cell: HTMLElement, activities: Activity[], day: string) {
    /*
    lesson: blue
    */
    if (activities) {
      cell.style.background = `linear-gradient( to bottom right, ${this.colors[activities[0].type - 1]} 20%, white)`;
      this.bindActivitiesToDates(activities, day);
    } else {
      cell.style.background = 'linear-gradient(to bottom right, rgba(103, 98, 98, 0.5) 20%, white)';
    }
  }

  private bindActivitiesToDates(activities: Activity[], day: string) {
    this.addEventFromServer(activities[0]);
    //get month and year from the view (current calendar page) and add day-of-month;
    var date = new Date(this.viewTitle);
    date.setDate(Number(day));
    this.DatesActivities.set(date.toLocaleDateString(), activities);
  }

  private isVisible(target: HTMLElement) {
    // Все позиции элемента
    var targetPosition = {
      top: window.pageYOffset + target.getBoundingClientRect().top,
      left: window.pageXOffset + target.getBoundingClientRect().left,
      right: window.pageXOffset + target.getBoundingClientRect().right,
      bottom: window.pageYOffset + target.getBoundingClientRect().bottom
    },
      // Получаем позиции окна
      windowPosition = {
        top: window.pageYOffset,
        left: window.pageXOffset,
        right: window.pageXOffset + document.documentElement.clientWidth,
        bottom: window.pageYOffset + document.documentElement.clientHeight
      };

    if (targetPosition.bottom > windowPosition.top && // Если позиция нижней части элемента больше позиции верхней чайти окна, то элемент виден сверху
      targetPosition.top < windowPosition.bottom && // Если позиция верхней части элемента меньше позиции нижней чайти окна, то элемент виден снизу
      targetPosition.right > windowPosition.left && // Если позиция правой стороны элемента больше позиции левой части окна, то элемент виден слева
      targetPosition.left < windowPosition.right) { // Если позиция левой стороны элемента меньше позиции правой чайти окна, то элемент виден справа
      // Если элемент полностью видно, то запускаем следующий код
      return this.isNotMuted(target);
    } else {
      // Если элемент не видно, то запускаем этот код
      return false;
    };
  }

  private isNotMuted(target: HTMLElement) {
    return !target.classList.contains('text-muted');
  }
}