import { Component } from '@angular/core';
import { NavController, NavParams, ModalOptions, ModalController } from 'ionic-angular';
import { BasePage } from '../basepage';
import { Activity, ActivityType } from '../../models/activities/activity';
import { ActivityDescComponent } from '../../components/activity-desc/activity-desc';
import { SpinnerComponent } from '../../components/spinner/spinner';
import { excursionComponent } from '../../components/excursions/excursion.component';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the MyActivitiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-activities',
  templateUrl: 'my-activities.html',
})
export class MyActivitiesPage extends BasePage {

  segment: any;

  

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private iap:InAppBrowser) {
    super();
    this.segment = "excursions";
  }

  private openDesc() {
    console.log();
    let opt: ModalOptions = {
      showBackdrop: false,

    }
    let act: Activity = {
      timeStart: {
        hours: 9,
        minutes: 10
      },
      timeEnd: {
        hours: 10,
        minutes: 10
      },
      groups: [],
      title: "Поездка в Эйлат",
      type: ActivityType.EXCURSION,
      description: "Двух-дневная поездка в Эйлат. С собой взять плавки, полотенце, 2 бутылки воды, удобную обувь..."

    }
     this.modalCtrl.create(ActivityDescComponent, { activity: act}, opt).present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyActivitiesPage');
  }
  private segmentChanged($event) {

  }

  private openLink(url: string){
    this.iap.create(
      url,
      "_blank",
      {}
    );
  }

  

}
