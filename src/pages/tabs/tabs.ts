import { Component } from '@angular/core';
import { TodayPage } from '../today/today';
import { TimetablePage } from '../timetable/timetable';
import { MyActivitiesPage } from '../my-activities/my-activities';
import { BasePage } from '../basepage';
import { Info } from '../info/info';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { Events } from 'ionic-angular';
import { ProfileComponent } from '../../components/profile/profile';

@Component({
  selector: 'my-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage extends BasePage {

  tab1Root = TodayPage;
  tab2Root = TimetablePage;
  tab3Root = MyActivitiesPage;
  tab4Root = Info;

  params: any;
  currentDate: Date = new Date();
  constructor(private modalCtrl: ModalController, private events: Events) {
    super();
    this.params = {
      date: this.currentDate
    }
    this.openProfile();
  }

  private openProfile() {
    this.events.subscribe("open:profile", () => {
      console.log('open');
      this.modalCtrl.create(ProfileComponent).present();
    });
  }
}
