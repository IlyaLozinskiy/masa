import { Subscription } from "rxjs";


export class BasePage {
    subscribtions: Subscription[];

    constructor() {
        this.subscribtions = new Array<Subscription>();
    }

    ngOnDestroy() {
        this.subscribtions.forEach(s => {
            s.unsubscribe();
        })
    }
}