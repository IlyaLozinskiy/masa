import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { BasePage } from '../basepage';
import { CallService } from '../../services/call';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { OpenMaps } from '../../services/maps';
import { ApiProvider } from '../../providers/api/api-provider';
import { Group } from '../../models/program/group';
import { Pupil } from '../../models/program/pupil';

/**
 * Generated class for the Info page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class Info extends BasePage {

  segment: any;

  changePass: boolean;

  group: Group;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private call: CallService,
    private alertCtrl: AlertController,
    private api: ApiProvider,
    private maps: OpenMaps
  ) {
    super();
    this.group = new Group();
    this.segment = "news";
  }

  ionViewDidLoad() {
    this.getGroup();
  }

  ionViewDidEnter() {
  }

  private segmentChanged($event) {

  }


  /*******News*******/


  /*******Contacts*******/
  private doCall(number: string, emergency: boolean) {

    if (emergency) {
      let alert = this.alertCtrl.create({
        title: `<small>Are you sure you want to call</small> ${number} <small>?</small>`,
        buttons: [
          {
            role: 'canel',
            text: 'Cancel'
          },
          {
            text: 'Call',
            handler: () => {
              alert.dismiss();
              this.call.doCall(number);
            }
          }
        ]
      });

      alert.present();

    } else {
      this.call.doCall(number);
    }
  }

  private openMaps(dest) {
    this.maps.openApp(dest);
  }

  presentActionSheet(item: Pupil) {
    let actionSheet = this.actionSheetCtrl.create({
      title: `${item.name} ${item.lastName}`,
      cssClass: 'masaActionSheet',
      buttons: [
        {
          icon: 'md-call',
          text: item.phone,
          handler: () => {
            this.call.doCall(item.phone);
            return false;
          }
        },
        {
          icon: 'md-mail',
          text: item.email,
          handler: () => {
            return false;
          }
        },

      ]
    });
    actionSheet.present();
  }

  /*******Group*******/
  private getGroup() {
    this.subscribtions.push(this.api.getGroup().subscribe(response => {
      this.group = response;
    }, error => {
      console.warn(error);
    }))
  }
}



