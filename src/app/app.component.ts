import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { FirebaseProvider } from '../providers/firebase/firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any = TabsPage;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private firebase: FirebaseProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.setupFirebase();
      statusBar.overlaysWebView(false);
      statusBar.backgroundColorByHexString("#ffffff");
      splashScreen.hide();
    });
  }

  private setupFirebase() {
    this.firebase.grantPerm().then((response) => {
      console.log(response);
      this.getToken();
      this.onTokenRefresh();
      this.subscribeToTopic('test');
      this.onNotificationArrived();
    }).catch((error) => {
      console.error(error);
      this.getToken();
      this.onTokenRefresh();
      this.subscribeToTopic('test');
      this.onNotificationArrived();
    });
  }

  private getToken() {
    this.firebase.getToken().then((response) => {
      console.log("getToken: ", response);

    }).catch((error) => {
      console.error(error);
    });
  }

  private onTokenRefresh() {
    this.firebase.onTokenRefresh().subscribe(response => {
      console.log("tokenRegresh: ", response);
    }, error => {
      console.error(error);

    }, () => {

    });
  }

  private onNotificationArrived() {
    this.firebase.onNotificationArrived().subscribe(response => {
      console.log("notification: ", response);

    }, error => {
      console.error(error);

    }, () => {

    });
  }

  private subscribeToTopic(topic: string) {
    this.firebase.subscribe(topic).then((response) => {
      console.log("subscribed to" + topic + ": ", response);
    }).catch((error) => {
      console.error(error);
    });
  }
}
