import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TodayPage } from '../pages/today/today';
import { TimetablePage } from '../pages/timetable/timetable';
import { TabsPage } from '../pages/tabs/tabs';
import { TimeProvider } from '../providers/time/time';
import { ProvidersModule } from '../providers/providers.module';
import { BasePage } from '../pages/basepage';
import { ComponentsModule } from '../components/components.module';
import { Info } from '../pages/info/info';
import { DirectivesModule } from '../directives/directives.module';
import { PipesModule } from '../pipes/pipes.module';
import { NgCalendarModule  } from 'ionic2-calendar';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { ServicesModule } from '../services/service.module';
import { MyActivitiesPage } from '../pages/my-activities/my-activities';
import { excursionComponent } from '../components/excursions/excursion.component'

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    TodayPage,
    TimetablePage,
    MyActivitiesPage,
    Info
  ],
  imports: [
    BrowserModule,
    ProvidersModule,
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    ServicesModule,
    NgCalendarModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    TodayPage,
    TimetablePage,
    MyActivitiesPage,
    Info
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TimeProvider,
    FirebaseProvider,
  ]
})
export class AppModule {}
